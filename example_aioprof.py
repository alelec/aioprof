import time
import aioprof

try:
    import asyncio
except ImportError:
    import uasyncio as asyncio

aioprof.enable()


async def quicker():
    while True:
        # This is being used as a standin for any blocking operation,
        # like a filesystem write, spi transfer, i2c scan etc.
        time.sleep_ms(1)

        await asyncio.sleep_ms(10)


async def slow():
    while True:
        # A much longer blocking operation
        time.sleep_ms(80)

        await asyncio.sleep_ms(10)


async def main():
    asyncio.create_task(slow())
    asyncio.create_task(quicker())

    await asyncio.sleep_ms(500)

    # print(aioprof.json())
    aioprof.report()


asyncio.run(main())
