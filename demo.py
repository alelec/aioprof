import uasyncio as asyncio
from machine import Pin, I2C
import aiorepl

async def blink():
    led = Pin("LED_BLUE")
    while True:
        await asyncio.sleep_ms(300)
        led.value(0 if led.value() else 1)

async def watch_for_switch():
    switch = Pin("SW", Pin.IN, Pin.PULL_UP)
    while True:
        await asyncio.sleep_ms(50)

        # Check if switch it pressed
        if switch.value() == 0:
            print("PRESSED!")

            # Start i2c scan
            asyncio.create_task(check_for_i2c())

            # Wait until the switch is released
            while switch.value() == 0:
                asyncio.sleep_ms(50)

async def check_for_i2c():
    while True:
        await asyncio.sleep_ms(0)
        I2C(1).scan()


async def amain():
    # Start the aiorepl task.
    t_repl = asyncio.create_task(aiorepl.task())
    asyncio.create_task(blink())
    asyncio.create_task(watch_for_switch())
    await t_repl

asyncio.run(amain())
